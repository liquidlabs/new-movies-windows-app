﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMovieReviews.ViewModels
{
    public class ItemDetailsViewModel : INotifyPropertyChanged
    {

        private string _lineId;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineId
        {
            get
            {
                return _lineId;
            }
            set
            {
                if (value != _lineId)
                {
                    _lineId = value;
                    NotifyPropertyChanged("LineId");
                }
            }
        }

        private string _lineRatingImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineRatingImage
        {
            get
            {
                return _lineRatingImage;
            }
            set
            {
                if (value != _lineRatingImage)
                {
                    _lineRatingImage = value;
                    NotifyPropertyChanged("LineRatingImage");
                }
            }
        }

        private string _lineSearchQuery;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineSearchQuery
        {
            get
            {
                return _lineSearchQuery;
            }
            set
            {
                if (value != _lineSearchQuery)
                {
                    _lineSearchQuery = value;
                    NotifyPropertyChanged("LineSearchQuery");
                }
            }
        }



        private string _lineTitle;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineTitle
        {
            get
            {
                return _lineTitle;
            }
            set
            {
                if (value != _lineTitle)
                {
                    _lineTitle = value;
                    NotifyPropertyChanged("LineTitle");
                }
            }
        }

        private string _lineImage;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineImage
        {
            get
            {
                return _lineImage;
            }
            set
            {
                if (value != _lineImage)
                {
                    _lineImage = value;
                    NotifyPropertyChanged("LineImage");
                }
            }
        }

        private string _lineCast;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineCast
        {
            get
            {
                return _lineCast;
            }
            set
            {
                if (value != _lineCast)
                {
                    _lineCast = value;
                    NotifyPropertyChanged("LineCast");
                }
            }
        }

        private string _lineReleaseDate;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineReleaseDate
        {
            get
            {
                return _lineReleaseDate;
            }
            set
            {
                if (value != _lineReleaseDate)
                {
                    _lineReleaseDate = value;
                    NotifyPropertyChanged("LineReleaseDate");
                }
            }
        }


        private string _lineGenre;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineGenre
        {
            get
            {
                return _lineGenre;
            }
            set
            {
                if (value != _lineGenre)
                {
                    _lineGenre = value;
                    NotifyPropertyChanged("LineGenre");
                }
            }
        }

        private string _lineRated;

        public string LineRated
        {
            get
            {
                return _lineRated;
            }
            set
            {
                if (value != _lineRated)
                {
                    _lineRated = value;
                    NotifyPropertyChanged("LineRated");
                }
            }
        }

        private string _lineRunningTime;

        public string LineRunningTime
        {
            get
            {
                return _lineRunningTime;
            }
            set
            {
                if (value != _lineRunningTime)
                {
                    _lineRunningTime = value;
                    NotifyPropertyChanged("LineRunningTime");
                }
            }
        }

        private string _lineSummary;

        public string LineSummary
        {
            get
            {
                return _lineSummary;
            }
            set
            {
                if (value != _lineSummary)
                {
                    _lineSummary = value;
                    NotifyPropertyChanged("LineSummary");
                }
            }
        }


        private string _lineRating;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineRating
        {
            get
            {
                return _lineRating;
            }
            set
            {
                if (value != _lineRating)
                {
                    _lineRating = value;
                    NotifyPropertyChanged("LineRating");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


        public ItemDetailsViewModel()
        {

        }

    }
}
