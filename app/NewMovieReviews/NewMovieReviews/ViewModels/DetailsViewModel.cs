﻿#define DEBUG

using NewMovieReviews.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewMovieReviews.ViewModels
{
    public class DetailsViewModel: INotifyPropertyChanged
    {
        private static MovieDetails movieDetails;

        /* Movie Details with Cast related Variables */
        public delegate void DetailsCallBack(MovieDetailsJsonWrapper detailsWrapper);
        private static ItemDetailsViewModel itemDetails;
        public ObservableCollection<ItemCastViewModel> CastItems { get; private set; }

        /* Movie Reviews related Variables */
        public delegate void ReviewsCallBack(MovieReviewsJsonWrapper detailsWrapper);
        private static ObservableCollection<ItemReviewsViewModel> itemReviews;
        private static int reviewsCount;
        public String ReviewsTitle
        {
            get
            {
                return reviewsCount + " Reviews";
            }
            set
            {
                reviewsCount = Convert.ToInt32(value);
            }

        }

        /* property for itemReviews */
        public ObservableCollection<ItemReviewsViewModel> ItemReviews
        {
            get
            {
                return itemReviews;
            }
            set
            {
                itemReviews = value;
            }

        }

        public ItemDetailsViewModel ItemDetails { 
            get 
            {
                return itemDetails;                   
            }

            set
            {
                if (value != null && value.LineId != itemDetails.LineId)
                {
                    itemDetails = value;
                }

            }
        }




        public DetailsViewModel()
        {
            if (movieDetails == null) {
                movieDetails = new MovieDetails();                
            }
            if (itemDetails == null)
            {
                itemDetails = new ItemDetailsViewModel();
            }
            if (CastItems == null)
            {
                CastItems = new ObservableCollection<ItemCastViewModel>();
            }
            if (itemReviews == null)
            {
                itemReviews = new ObservableCollection<ItemReviewsViewModel>();
            }
        }

        public void loadMovieDetails(String id)
        {
            // asynchronously request the reviews to be loaded
            this.loadMovieReviews(id);

            DetailsCallBack callback = new DetailsCallBack(this.loadDetails);
            movieDetails.getMovieDetailsAsync(callback, id);
        }



        public void loadDetails(MovieDetailsJsonWrapper detailsWrapper)
        {
            //StringBuilder casts = new StringBuilder();
            CastItems.Clear();
            foreach (AbridgedCast cast in detailsWrapper.abridged_cast)
            {
                //casts.Append(cast.name).Append(", ");
                if (cast != null)
                {
                    CastItems.Add(new ItemCastViewModel()
                    {
                        LineName = cast.name,
                        LineStageName = cast.characters != null && cast.characters.Count > 0 ? cast.characters[0] : "..."

                    });
                }
            }

            String ratingImage = detailsWrapper.ratings.audience_score >= 50 ? Movies.RatingsHigh : Movies.RatingsLow;
            String releaseDate = Movies.FormatReleaseDate(Movies.ReleaseDate(detailsWrapper.release_dates.theater));

            itemDetails.LineId = detailsWrapper.id + "";
            itemDetails.LineTitle = detailsWrapper.title;
            itemDetails.LineImage = detailsWrapper.posters.profile;
            itemDetails.LineRated = "Rated: " + detailsWrapper.mpaa_rating;
            itemDetails.LineRating = detailsWrapper.ratings.audience_score + "%";
            itemDetails.LineGenre = "Genre: " + string.Join(", ", detailsWrapper.genres);
           // itemDetails.LineCast = casts.ToString();
            itemDetails.LineSummary = detailsWrapper.synopsis;
            itemDetails.LineRunningTime = "Running Time: " + detailsWrapper.runtime + " min";
            itemDetails.LineRatingImage = ratingImage;
            itemDetails.LineReleaseDate = releaseDate;

            

            DateTime dt = Movies.ReleaseDate(detailsWrapper.release_dates.theater);
            itemDetails.LineSearchQuery = detailsWrapper.title + " (" + String.Format("{0:yyyy}", dt) + ")";

            System.Diagnostics.Debug.WriteLine("Movie Id => " + itemDetails.LineId);

        }

        public void loadMovieReviews(String id)
        {
            ReviewsCallBack callback = new ReviewsCallBack(this.loadReviews);
            movieDetails.getMovieReviewsAsync(callback, id);
        }


        public void loadReviews(MovieReviewsJsonWrapper reviewsWrapper)
        {
            itemReviews.Clear();
            foreach (Review review in reviewsWrapper.reviews)
            {
                DateTime dt = Movies.ReleaseDate(review.date);

                ItemReviewsViewModel reviewModel = new ItemReviewsViewModel() 
                {
                     LineName = review.critic + ":",
                     LineComment = review.quote + "\nReviewed on: " + String.Format("{0: MMMM d, yyyy}", dt),
                     LineReviewDate = review.date
                };


                if (review.original_score != null && review.original_score.Trim().Equals(""))
                {
                    reviewModel.LineComment = reviewModel.LineComment + "\nRated: " + review.original_score;
                }

                ItemReviews.Add(reviewModel);

            }
            System.Diagnostics.Debug.WriteLine("Total Movie Reviews: " + reviewsWrapper.total);
            reviewsCount = reviewsWrapper.total;
            NotifyPropertyChanged("ItemReviews");
            NotifyPropertyChanged("ReviewsTitle");
        }



        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }


    }
}
