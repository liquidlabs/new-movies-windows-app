﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace NewMovieReviews.ViewModels
{
    public class ItemCastViewModel : INotifyPropertyChanged
    {

        private string _lineStageName;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineStageName
        {
            get
            {
                return " as " + _lineStageName;
            }
            set
            {
                if (value != _lineStageName)
                {
                    _lineStageName = value;
                    NotifyPropertyChanged("LineStageName");
                }
            }
        }


        private string _lineName;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string LineName
        {
            get
            {
                return _lineName;
            }
            set
            {
                if (value != _lineName)
                {
                    _lineName = value;
                    NotifyPropertyChanged("LineName");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}