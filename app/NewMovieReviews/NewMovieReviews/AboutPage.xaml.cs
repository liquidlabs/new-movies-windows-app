﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;

namespace NewMovieReviews
{
    public partial class AboutPage : PhoneApplicationPage
    {
        private static String AuthorUrl = "http://www.liquidlabs.ca/newmovies";

        public AboutPage()
        {
            InitializeComponent();
        }


        public void OnAuthorTap(object sender, RoutedEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(AuthorUrl);
            webBrowserTask.Show(); 
        }
    }
}