﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.UI.Popups;
using NewMovieReviews.Resources;
using NewMovieReviews.ViewModels;

namespace NewMovieReviews
{
    public partial class MainPage : PhoneApplicationPage
    {
        private ListBox pressedListBox;

        public delegate void LoadErrorCallback(String error);


        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (pressedListBox != null)
            {
                pressedListBox.SelectedItem = null;
            }

            if (!App.ViewModel.IsDataLoaded)
            {
                LoadErrorCallback callback = new LoadErrorCallback(this.DisplayError);
                App.ViewModel.LoadData(callback);
            }
        }


        public void DisplayError(String Error)
        {
            //var messageDialog = new MessageDialog("No internet connection has been found.");
            MessageBox.Show("Error Loading Movies. Please Refresh to Try Again");
            
        }


        public void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            pressedListBox = (sender as ListBox);
            ItemViewModel data = pressedListBox.SelectedItem as ItemViewModel;
            if (data != null)
            {
                System.Diagnostics.Debug.WriteLine("Movie ID [] " + data.LineId);
                NavigationService.Navigate(new Uri("/DetailsPage.xaml?id=" + data.LineId, UriKind.Relative));
            }
        }

        private void OnPivotLoaded(object sender, RoutedEventArgs e)
        {
            string pivotIndex = "";

            if (NavigationContext.QueryString.TryGetValue("id", out pivotIndex))
            {
                //-1 because the Pivot is 0-indexed, so pivot item 2 has an index of 1
                PagePivot.SelectedIndex = Convert.ToInt32(pivotIndex) - 1;
                
            }
        }


        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}