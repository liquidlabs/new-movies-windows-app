﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using NewMovieReviews.ViewModels;
using NewMovieReviews.Models;
using Microsoft.Phone.Tasks;

namespace NewMovieReviews
{
    public partial class DetailsPage : PhoneApplicationPage
    {
        public delegate void TrailerCallback(String trailerUrl);

        public DetailsPage()
        {
            InitializeComponent();
            DataContext = App.DetailsModel;
        }


        private void OnDetailsPageLoad(object sender, RoutedEventArgs e)
        {
            
            // check if the current detailed View Item is already in WatchList, then 
            // change the + WatchList text to - WatchList
            //App.ViewModel.WatchlistItems.Contains
        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //if (e.NavigationMode == NavigationMode.Back) return;

            string movieId = ""; 
            if (NavigationContext.QueryString.TryGetValue("id", out movieId))
            {
                System.Diagnostics.Debug.WriteLine("Movie Id Passed => " + movieId);
                App.DetailsModel.loadMovieDetails(movieId);

                if (App.ViewModel.WatchlistItems.Contains(new ItemViewModel() { LineId = movieId }))
                {
                    WatchListBtn.Content = "- Watchlist";
                }
                else
                {
                    WatchListBtn.Content = "+ Watchlist";
                }
            }
        }

        public void OnWatchlistClick(object sender, RoutedEventArgs e)
        {
            Button watchlistBtn = (Button) sender;

            System.Diagnostics.Debug.WriteLine("Adding to Watch List : " + watchlistBtn.Tag);
            MainViewModel model = MainViewModel.getInstance();
            // Get the Movie, and then add it to Database!
            if (watchlistBtn.Content.ToString().StartsWith("+"))
            {
                model.AddWatchlistItem(watchlistBtn.Tag + "");
            }
            else
            {
                model.DeleteWatchlistItem(watchlistBtn.Tag + "");
            }

            NavigationService.Navigate(new Uri("/MainPage.xaml?id=4", UriKind.Relative));

            // Assuming its successfully Added, now do something with the button!
        }

        public void OnTrailerClick(object sender, RoutedEventArgs e)
        {
            Button btn = (Button) sender;
            TrailerCallback callback = new TrailerCallback(this.OpenTrailerUrl);

            System.Diagnostics.Debug.WriteLine("Search Query: " + btn.Tag);
            // NavigationService.Navigate(new Uri("/ReviewsPage.xaml?id=" + btn.Tag, UriKind.Relative));
            /* Make a request to the Trailer Model, and redirect User to the browser URL via browser! */
            MovieTrailer.getTrailerUrl(callback, btn.Tag + "");
        }

        public void OpenTrailerUrl(String trailerUrl)
        {
            System.Diagnostics.Debug.WriteLine("Trailer Url: " + trailerUrl);
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri(trailerUrl);
            webBrowserTask.Show();             
        }
    }
}