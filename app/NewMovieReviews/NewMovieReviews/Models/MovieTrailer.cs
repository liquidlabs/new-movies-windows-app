﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Net;

namespace NewMovieReviews.Models
{
    class MovieTrailer
    {
        public static readonly String SearchUrl = "https://gdata.youtube.com/feeds/api/videos?max-results=1&q=";


        public MovieTrailer()
        {

        }

        /**
         * <summary>
         *  Returns the YouTube Url of a Movie (passed on to the query)
         *  The Url is of the format: https://www.youtube.com/watch?v=movie-id 
         * </summary>
         * <param>query: The Query to be passed for getting YouTube Url </query>
         */
        public static void getTrailerUrl(DetailsPage.TrailerCallback callback, String query)
        {
            String searchUrl = SearchUrl + System.Net.HttpUtility.UrlEncode(query);
           

            WebClient client = new WebClient();
            client.DownloadStringCompleted +=
                   new DownloadStringCompletedEventHandler (
                        delegate(object sender, DownloadStringCompletedEventArgs e)
                        {
                            XDocument doc = XDocument.Parse(e.Result);
                            foreach (var entry in doc.Descendants(XName.Get("entry", "http://www.w3.org/2005/Atom"))) 
                            {
                                foreach (var i in entry.Descendants(XName.Get("link", "http://www.w3.org/2005/Atom")))
                                {
                                    if (i.HasAttributes)
                                    {
                                        if (i.Attribute("rel").Value.Equals("alternate") && i.Attribute("type").Value.Equals("text/html"))
                                        {
                                            // this is what I need!
                                            String url = i.Attribute("href").Value;
                                            System.Diagnostics.Debug.WriteLine(url);
                                            callback(url);
                                            break;
                                        }
                                    }
                                }
                                // should only execute once!
                                break;
                            }

                        }
                    );
            client.DownloadStringAsync(new Uri(searchUrl, UriKind.Absolute));
        }


    }
}
