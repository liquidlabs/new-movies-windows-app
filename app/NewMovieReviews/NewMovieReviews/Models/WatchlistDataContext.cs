﻿/** 
 * Copyright (c) 2011 Microsoft Corporation.  All rights reserved.  
 * 
 */
using System;
using System.ComponentModel;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace NewMovieReviews.Model
{
    // 
    public class WatchlistDataContext : DataContext
    {
        // Pass the connection string to the base class.
        public WatchlistDataContext(string connectionString)
            : base(connectionString)
        { }

        // Specify a table for the to-do items.
        public Table<MovieItem> Items;
    }

    [Table]
    public class MovieItem : INotifyPropertyChanged, INotifyPropertyChanging
    {

        // Define ID: private field, public property, and database column.
        private int _MovieItemId;

        [Column(IsPrimaryKey = true)]
        public int MovieItemId
        {
            get { return _MovieItemId; }
            set
            {
                if (_MovieItemId != value)
                {
                    NotifyPropertyChanging("MovieItemId");
                    _MovieItemId = value;
                    NotifyPropertyChanged("MovieItemId");
                }
            }
        }

        // Define item name: private field, public property, and database column.
        private string _itemTitle;

        [Column]
        public string ItemTitle
        {
            get { return _itemTitle; }
            set
            {
                if (_itemTitle != value)
                {
                    NotifyPropertyChanging("ItemTitle");
                    _itemTitle = value;
                    NotifyPropertyChanged("ItemTitle");
                }
            }
        }

        // Define item name: private field, public property, and database column.
        private string _itemReleaseDate;

        [Column]
        public string ItemReleaseDate
        {
            get { return _itemReleaseDate; }
            set
            {
                if (_itemReleaseDate != value)
                {
                    NotifyPropertyChanging("ItemReleaseDate");
                    _itemReleaseDate = value;
                    NotifyPropertyChanged("ItemReleaseDate");
                }
            }
        }

        private string _itemImage;

        [Column]
        public string ItemImage
        {
            get { return _itemImage; }
            set
            {
                if (_itemImage != value)
                {
                    NotifyPropertyChanging("ItemImage");
                    _itemImage = value;
                    NotifyPropertyChanged("ItemImage");
                }
            }
        }


        // Define item name: private field, public property, and database column.
        private string _itemCasts;

        [Column]
        public string ItemCasts
        {
            get { return _itemCasts; }
            set
            {
                if (_itemCasts != value)
                {
                    NotifyPropertyChanging("ItemCasts");
                    _itemCasts = value;
                    NotifyPropertyChanged("ItemCasts");
                }
            }
        }

        // Define item name: private field, public property, and database column.
        private string _itemRating;

        [Column]
        public string ItemRating
        {
            get { return _itemRating; }
            set
            {
                if (_itemRating != value)
                {
                    NotifyPropertyChanging("ItemRating");
                    _itemRating = value;
                    NotifyPropertyChanged("ItemRating");
                }
            }
        }

       


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        // Used to notify that a property changed
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region INotifyPropertyChanging Members

        public event PropertyChangingEventHandler PropertyChanging;

        // Used to notify that a property is about to change
        private void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion
    }


    


}
